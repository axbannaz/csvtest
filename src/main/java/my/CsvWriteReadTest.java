package my;

/**
 * @author ashraf
 *
 */
public class CsvWriteReadTest
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if (args.length < 1)
			return;

		for (String arg: args) {
			System.out.print(arg + ";");
		}

//		String fileName = System.getProperty("user.home") + "/student.csv";
		String fileName = args[0];

//		System.out.println("Write CSV file:");
//		CsvFileWriter.writeCsvFile(fileName);

		System.out.println("\nRead CSV file:");
		CsvFileReader.readCsvFile(fileName);
	}
}